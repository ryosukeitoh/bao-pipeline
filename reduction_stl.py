#!/usr/bin/python3

import pyraf,iraf
import sys,os,time,math,random,re
import urllib,getopt,string,optparse,numpy
import astropy
import astropy.io.fits
import astropy.coordinates
import astropy.stats
import logging
import lacosmic



class reduction():

    def __init__(self):
        self.astrometry  = "timeout 60 /usr/bin/solve-field %s -N %s -p -O "
        self.astrometry += "-L %s -H %s -u arcsecperpix "
        self.astrometryRD= self.astrometry + " --ra %s --dec %s --radius %s"
        self.cr_width    = 50
        self.ra          = False
        self.dec         = False
        self.inputcoor   = "no"
        self.overwrite   = "no"
        self.fitsext     = [".FITS",".FTS",".FIT",".G",".R",".B"]
        self.wcstrialNum = 2
        self.outputdir   = False
        self.writehead   = True
        self.d_a         = 0.00440435274321
        self.d_b         = -1.04071796764
        self.biascount   = {1:105.13306,
                            0:1898.5841}
        fdir = "/Input_your_flat_directry/"
        self.masterflat  = {"B-wo":fdir+"flat_B_woRed.fits",
                            "V-wo":fdir+"flat_V_woRed.fits",
                            "R-wo":fdir+"flat_R_woRed.fits",
                            "I-wo":fdir+"flat_I_woRed.fits",
                            "L-wo":fdir+"flat_RGB_L_woRed.fits",
                            "Red-wo":fdir+"flat_RGB_R_woRed.fits",
                            "Blue-wo":fdir+"flat_RGB_B_woRed.fits",
                            "Green-wo":fdir+"flat_RGB_G_woRed.fits",
                            "Clear-wo":fdir+"flat_RGB_L_woRed.fits",
                            "B-w":fdir+"flat_B_wRed.fits",
                            "V-w":fdir+"flat_V_wRed.fits",
                            "R-w":fdir+"flat_R_wRed.fits",
                            "I-w":fdir+"flat_I_wRed.fits",
                            "L-w":fdir+"flat_RGB_L_wRed.fits",
                            "Red-w":fdir+"flat_RGB_R_wRed.fits",
                            "Blue-w":fdir+"flat_RGB_G_wRed.fits",
                            "Green-w":fdir+"flat_RGB_G_wRed.fits",
                            "Clear-w":fdir+"flat_RGB_L_wRed.fits"}
        pw_UCAC  = "./"
        if os.path.exists(pw_UCAC)==False:
            print("No UCAC-4 catalog")
            return False
        sys.path.append(pw_UCAC)
        self.sexparam   = "default.param"
        self.th_starnum = 15.0
        
    def ra2deg(self,RA1):
        SS1    = RA1.split(":")
        rt     =  float(SS1[0])*15 + float(SS1[1])*15/60.0 +\
                  float(SS1[2])*15/3600.0
        return rt
    
    def dec2deg(self,DEC1):
        SS1 = DEC1.split(":")
        if float(SS1[0]) >= 0:
            DECDEG1 = (float(SS1[0]) +
                       float(SS1[1])/60.0 +
                       float(SS1[2])/3600.0)
        else:
            DECDEG1 = (float(SS1[0]) -
                       float(SS1[1])/60.0 -
                       float(SS1[2])/3600.0)
        return DECDEG1

    def readlist(self,listf):
        k = open(listf,"r")
        r = k.read().split()
        k.close()
        return r
    
    def check_filter_by_name(self,fits):
        fd   = astropy.io.fits.open(fits)
        fl   = fd[0].header["FILTER"]
        fd.close()
        self.filname= "unknown"
        FilCan  = ["B","V","R","RC","I","IC","H","HA","HALPHA"]
        fc      = {"B":"B","V":"V","R":"R","RC":"R","I":"I","IC":"I",
                   "H":"Halpha","HA":"Halpha","HALPHA":"Halpha"}
        #RGBname = {"Green":"RGB-G","Blue":"RGB-G","Red":"RGB-R"}
        if "Clear" not in fl:
            self.filname  =fl
        else:
            con = re.split('[,._-]',fits)
            for c in con:
                if c.upper() in FilCan:
                    self.filname = fc[c.upper()]



    def check_reducer(self,objfits):
        fd   = astropy.io.fits.open(objfits)
        D    = fd[0].data
        x,y  = D.shape
        cenD = D[int(y/2.-self.cr_width):int(y/2.+self.cr_width),
                 int(x/2.-self.cr_width):int(x/2.+self.cr_width)]
        corD = D[0:self.cr_width,
                 0:self.cr_width]
        mcenD = numpy.median(cenD)
        mcorD = numpy.median(corD)
        rD    = mcenD/mcorD
        fd.close()
        print(rD)
        if rD > 1.2: # w/ reducer mode
            self.Rmode   = "w"
            self.Pscale  = [0.81,0.89] # 0.8 -- 0.9 arcsec/pixel
            self.rad     = 0.30      # ROI [degree]
        else:        # w/o reducer mode
            self.Rmode   = "wo"
            self.Pscale  = [0.38,0.43] # 0.3 -- 0.5 arcsec/pixel
            self.rad     = 0.15      # ROI [degree]


    def write_fil_red(self,fits):
        if self.Rmode == "wo":
            os.system("/usr/bin/sethead %s RED-VIG=%s"%(fits,"no"))
        else:
            os.system("/usr/bin/sethead %s RED-VIG=%s"%(fits,"yes"))
        os.system("/usr/bin/sethead %s FIL-NAME=%s"%(fits,self.filname))            
    def newfitsname(self,inputfits,sfix):
        if self.outputdir:
            bn      = os.path.splitext(os.path.basename(inputfits))[0]
            newfits = os.path.join(self.outputdir,bn+sfix)
        else:
            bn      = os.path.splitext(inputfits)[0]
            newfits = bn + sfix
        return newfits
    
    def dark_subtraction(self,objfits,darkfits):
        newfits = self.newfitsname(objfits,"_dk.fits")
        if os.path.exists(newfits) and self.overwrite=="no":return newfits
        if os.path.exists(newfits):os.system("rm -f %s"%newfits)
        iraf.imarith(operand1=objfits,op="-",
                     operand2=darkfits,result=newfits)
        time.sleep(0.1)
        os.system("/usr/bin/sethead %s DARK=%s"%(newfits,darkfits))
        return newfits

    def check_soft(self,soft):
        if   soft == "SBIG Win CCDOPS Version 5.40J-NT":s = 0
        elif soft == "CCDOPS Version 5.40J-NT":s = 0
        elif soft == "WinOPS Ver 5.40J-NT":s = 0
        elif soft == "SBIG Win CCDOPS Version 5.47J Build 11-NT":s = 1
        elif soft == "WinOPS Ver 5.47J Build 11-NT":s = 1
        elif soft == "CCDSoft Version 5.00.159":s = 1
        else:s = 1
        return s
    
    def master_dark_subtraction(self,objfits):
        newfits = self.newfitsname(objfits,"_dk.fits")
        if os.path.exists(newfits) and self.overwrite=="no":return newfits
        if os.path.exists(newfits):os.system("rm -f %s"%newfits)
        fd   = astropy.io.fits.open(objfits)
        exp  = fd[0].header["EXPTIME"]
        temp = fd[0].header["CCD-TEMP"]+273.15
        soft = fd[0].header["SWCREATE"]
        binx = fd[0].header["XBINNING"]
        sn   = self.check_soft(soft)
        fd.close()
        dA     = self.d_a *temp + self.d_b
        dvalue = (dA*exp)*binx**2 + self.biascount[sn] 
        iraf.imarith(operand1=objfits,op="-",
                     operand2=dvalue,result=newfits)
        time.sleep(0.1)
        os.system("/usr/bin/sethead %s dark=%s"%(newfits,dvalue))
        return newfits

    def flat_uniformity(self,objfits):    
        fd  = astropy.io.fits.open(objfits)
        D   = fd[0].data
        std = astropy.stats.mad_std(D)
        fd.close()
        return std
    
    def flat_correction(self,objfits,flatfits):
        newfits = self.newfitsname(objfits,"_fl.fits")
        if os.path.exists(newfits) and self.overwrite=="no":return newfits
        if os.path.exists(newfits):os.system("rm -f %s"%newfits)
        iraf.imarith(operand1=objfits,op="/",
                     operand2=flatfits,result=newfits)
        v = self.flat_uniformity(newfits)
        os.system("/usr/bin/sethead %s FLAT=%s"%(newfits,
                                                 os.path.basename(flatfits)))
        os.system("/usr/bin/sethead %s FLATTEN=%s"%(newfits,v))
        return newfits
    
    def master_flat_correction(self,objfits):
        newfits = self.newfitsname(objfits,"_fl.fits")
        fd  = astropy.io.fits.open(objfits)
        D   = fd[0].data
        stdL,bL = [],[]
        for band in self.masterflat.keys():
            al = astropy.io.fits.open(self.masterflat[band])
            fD = al[0].data
            cD = D/fD
            stdL.append(astropy.stats.mad_std(cD))
            al.close()
            bL.append(band)
        Id       = stdL.index(min(stdL))
        flatfits = self.masterflat[bL[Id]]
        fd.close()
        if os.path.exists(newfits) and self.overwrite=="no":return newfits
        if os.path.exists(newfits):os.system("rm -f %s"%newfits)
        iraf.imarith(operand1=objfits,op="/",
                     operand2=flatfits,result=newfits)
        v = self.flat_uniformity(newfits)
        os.system("/usr/bin/sethead %s FLAT=%s"%(newfits,
                                                 os.path.basename(flatfits)))
        os.system("/usr/bin/sethead %s FLATTEN=%s"%(newfits,v))
        return newfits
    
    def remove_cosmic(self,objfits):
        nfits = self.newfitsname(objfits,"_lc.fits")
        if os.path.exists(nfits) and self.overwrite=="no":return nfits
        if os.path.exists(nfits):os.system("rm -f %s"%nfits)
        fd   = astropy.io.fits.open(objfits)
        D    = fd[0].data
        H    = fd[0].header
        nD    = lacosmic.lacosmic(D,2.0,3.0,3.0,
                                  readnoise=10.0,
                                  effective_gain=1.0)[0]
        hdu     = astropy.io.fits.PrimaryHDU(nD,H)
        hdulist = astropy.io.fits.HDUList([hdu])
        hdulist.writeto(nfits,output_verify='ignore')
        fd.close()
        return nfits

    
    def sextract_param_starcount(self):
        k = open(self.sexparam,"w")
        k.write("X_IMAGE\n")
        k.write("Y_IMAGE\n")
        k.close()

    def count_star_num(self,objfits):
        import phot_all
        self.ph = phot_all.Phot(objfits)
        self.sextract_param_starcount()
        self.ph.sextract_conv()
        os.system(self.ph.sextract+" "+objfits)
        if os.path.exists(self.ph.sextmp)==False:return False
        resL     = self.ph.read_sextcat(self.ph.sextmp)
        if (len(resL)) > self.th_starnum:return True
        else:return False
        
        

    def wcs_astrometry(self,objfits):
        wcsfits = self.newfitsname(objfits,"_wcs.fits")
        if os.path.exists(wcsfits) and self.overwrite=="no":
            return wcsfits
        if os.path.exists(wcsfits):os.system("rm -f %s"%wcsfits)
        if self.ra:
            print(self.ra,self.dec)
            print(self.astrometryRD%(objfits,wcsfits,
                                     self.Pscale[0],self.Pscale[1],
                                     self.ra,self.dec,self.rad))
            os.system(self.astrometryRD%(objfits,wcsfits,
                                         self.Pscale[0],self.Pscale[1],
                                         self.ra,self.dec,self.rad))
        else:
            print(self.ra,self.dec)
            os.system(self.astrometry%(objfits,wcsfits,
                                       self.Pscale[0],self.Pscale[1]))
        if os.path.exists(wcsfits):
            if self.inputcoor == "no":
                hd = astropy.io.fits.open(wcsfits)
                self.ra  = hd[0].header["CRVAL1"]
                self.dec = hd[0].header["CRVAL2"]
                hd.close()
            return wcsfits
        else:
            if self.inputcoor == "no":
                self.ra,self.dec = False,False
            return False

    def rm_wcstempfile(self,lcfits):
        p0 = [lcfits.replace(".fits","-indx.xyls"),
              lcfits.replace(".fits",".axy"),
              lcfits.replace(".fits",".corr"),
              lcfits.replace(".fits",".match"),
              lcfits.replace(".fits",".rdls"),
              lcfits.replace(".fits",".solved"),
              lcfits.replace(".fits",".wcs")]
        for p in p0:os.system("rm -f %s"%p)
        
if __name__ == "__main__":

    usage  = '\n$ %s obj-fits(or list) \n\n'%sys.argv[0]
    usage += 'Reduction (Dark, Flat, WCS)\n'
    parser = optparse.OptionParser(usage=usage)
    parser.set_defaults(verbose=True)
    parser.add_option("--ra",
                      default=False,
                      help="center R.A. value for WCS astrometry [dd.d or hh:mm:ss]")    
    parser.add_option("--dec",
                      default=False,
                      help="center Decl. value for WCS astrometry [dd.d or dd:mm:ss]")
    parser.add_option("-w", "--no_wcs",
                      action="store_true",
                      help="without astrometry (no WCS headers)")
    parser.add_option("-o", "--overwrite",
                      action="store_true",
                      help="overwrite fits")
    parser.add_option("--no-write-header",dest="nonwrite",
                      default=False,
                      action="store_true",
                      help="overwrite fits")
    parser.add_option("-f", "--flat", dest="flatfits",
                      help="flat fits file", metavar="FILE")
    parser.add_option("-d", "--dark", dest="darkfits",
                      help="dark fits file", metavar="FILE")
    parser.add_option("--output-dir", dest="outdir",
                      help="output directry", metavar="FILE")
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit()
    (opts, args)   = parser.parse_args()

    rd     = reduction()

    infits = sys.argv[1]

    if opts.ra and opts.dec:rd.inputcoor = "yes"
    if opts.ra:
        if ":" in opts.ra: rd.ra  = rd.ra2deg(opts.ra)
        else:              rd.ra  = float(opts.ra)
    if opts.dec:
        if ":" in opts.dec:rd.dec = rd.dec2deg(opts.dec)
        else:              rd.dec = float(opts.dec)
    if opts.overwrite:rd.overwrite = "yes"
    if opts.flatfits:flat=opts.flatfits
    else:flat=False
    if opts.darkfits:dark=opts.darkfits
    else:dark=False
    if opts.outdir:rd.outputdir=opts.outdir
    if opts.nonwrite:
        rd.writehead = False
    ext  = os.path.splitext(infits)[1]
    if ext.upper() in rd.fitsext:fitsL = [infits]
    else:fitsL = rd.readlist(infits)
    
    for fits in fitsL:
        rd.check_reducer(fits)
        rd.check_filter_by_name(fits)
        print(rd.Rmode,"reducer")
        if dark:dk  = rd.dark_subtraction(fits,dark)
        else:   dk  = rd.master_dark_subtraction(fits)
        if flat:fl  = rd.flat_correction(dk,flat)
        else:   fl  = rd.master_flat_correction(dk)
        os.system("rm -f %s"%dk)
        lc  = rd.remove_cosmic(fl)
        os.system("rm -f %s"%fl)
        if rd.writehead:rd.write_fil_red(lc)
        jd = rd.count_star_num(lc)
        if jd==False:continue
        
        if opts.no_wcs:continue
        
        cnt = 0
        while cnt <= rd.wcstrialNum:
            wcs = rd.wcs_astrometry(lc)
            if wcs:break
            cnt += 1
        if wcs==False:
            cnt = 0
            if rd.Rmode == "wo":
                rd.Rmode   = "w"
                rd.Pscale  = [0.81,0.89] # 0.8 -- 0.9 arcsec/pixel
                rd.rad     = 0.30      # ROI [degree]
            else:
                rd.Rmode   = "wo"
                rd.Pscale  = [0.38,0.43] # 0.3 -- 0.5 arcsec/pixel
                rd.rad     = 0.15      # ROI [degree]
            while cnt <= rd.wcstrialNum:
                wcs = rd.wcs_astrometry(lc)
                if wcs:break
                cnt += 1
        rd.rm_wcstempfile(lc)
        os.system("rm -f %s"%lc)
